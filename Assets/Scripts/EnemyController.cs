using UnityEngine;
using UnityEngine.AI;
using System.Collections;
using AdvancedDissolve_Example;

public class EnemyController : MonoBehaviour {

	public float lookRadius = 10f;

	public Transform target;
	NavMeshAgent agent;
    public Animator animator;
    public PlayerController PlayerControllerScript;
    public crossScript CrossScriptScript;

    public bool isPraying;

    void Start()
	{
		agent = GetComponent<NavMeshAgent>();
        animator = this.GetComponent<Animator>();
    }

	void Update ()
	{
		float distance = Vector3.Distance(target.position, transform.position);
        if (CrossScriptScript.prayIntensity >= 99 && isPraying)
        {
            animator.SetInteger("state", 5); // ������
            this.GetComponent<Controller_Mask_XYZ_Axis>().offset -= 0.002f;
        }
        else
        {
            if (distance <= lookRadius)
            {
                agent.SetDestination(target.position);
                lookRadius = 4f;
                if (!isPraying)
                {
                    animator.SetInteger("state", 2); // ���
                    if (distance <= agent.stoppingDistance)
                    {
                        animator.SetInteger("state", 3); // �����
                    }
                }
                else
                {
                    animator.SetInteger("state", 4); // ������
                }
            }
            else
            {
                animator.SetInteger("state", 0); // idle
            }

            if (animator.GetInteger("state") == 3 || animator.GetInteger("state") == 4 || animator.GetInteger("state") == 5)
            {
                agent.isStopped = true;
            }
            else
            {
                agent.isStopped = false;
            }
        }
	}

    void FaceTarget ()
	{
		Vector3 direction = (target.position - transform.position).normalized;
		Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
		transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }

	void OnDrawGizmosSelected ()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position, lookRadius);
	}
}
