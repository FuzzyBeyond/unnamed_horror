﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float walkSpeed = 2;
	public float runSpeed = 6;
    public float gravity = -5;
    public float jumpHeight = 0.1f;
    [Range(0,1)]
    public float airControlPercent;

	public float turnSmoothTime = 0.2f;
	float turnSmoothVelocity;

	public float speedSmoothTime = 0.1f;
    public int jopa = 0;
	float speedSmoothVelocity;
	float currentSpeed;
    float velocityY;

	Animator animator;
	Transform cameraT;
    Rigidbody rgbody;

    CharacterController controller;

    public bool praying;
    public bool shooting;
    public crossScript cross;
    public EnemyController EnemyControllerScript;

    float additiveFloatY;
    float additiveFloatX;

    [SerializeField] public GameObject weapon;
    public GameObject uiBullets;

    void Start () {
		animator = GetComponent<Animator> ();
		cameraT = Camera.main.transform;
        rgbody = GetComponent<Rigidbody>();
        controller = GetComponent<CharacterController>();
        additiveFloatY = 0;
        additiveFloatX = 0;
        
        EquipWeapon(weapon);
        weapon.SetActive(false);  //Временная
    }

    private void OnEnable()
    {
        FightsEventHandler.OnAttack += SmthAboutShoot;
    }

    private void OnDestroy()
    {
        FightsEventHandler.OnAttack -= SmthAboutShoot;
    }

    public void EquipWeapon(GameObject _weapon)
    {
        FightsEventHandler.ChangeWeapon(_weapon.GetComponent<BaseRangeWeapon>());
    }


    private void SmthAboutShoot()
    {
        //Debug.Log("-патрон =(");
    }

    void Update () {

        if (Input.GetMouseButtonDown(0))
        {
            if (weapon != null)
                weapon.GetComponent<IWeapon>().Fire();
            else
                Debug.Log("АРУЖЕЕ ДАЙ");
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            EquipWeapon(weapon);
        }

        Vector2 input = new Vector2 (Input.GetAxisRaw ("Horizontal"), Input.GetAxisRaw ("Vertical"));
		Vector2 inputDir = input.normalized;

        var x = Input.GetAxis("Horizontal");
        var y = Input.GetAxis("Vertical");

        bool running = Input.GetKey(KeyCode.LeftShift);
        bool jumping = Input.GetKeyDown(KeyCode.Space);

        float targetSpeed = ((running) ? runSpeed : walkSpeed) * inputDir.magnitude;
        currentSpeed = Mathf.SmoothDamp(currentSpeed, targetSpeed, ref speedSmoothVelocity, GetModifiedSmoothTime(speedSmoothTime));

        velocityY += Time.deltaTime * gravity;

        Vector3 velocity = transform.forward * currentSpeed + Vector3.up * velocityY;
        controller.Move(velocity * Time.deltaTime);
        currentSpeed = new Vector2(controller.velocity.x, controller.velocity.z).magnitude;

        if (controller.isGrounded)
        {
            velocityY = 0;
        }

        float animationSpeedPercent = ((running) ? currentSpeed/runSpeed : currentSpeed/walkSpeed * .5f);

        if (Input.GetKeyDown(KeyCode.G))
        {
            praying = !praying;
            cross.enableCross(praying);
        }

        if (Input.GetMouseButtonDown(1))
        {
            shooting = !shooting;
            if (shooting)
            {
                FightsEventHandler.ChangeZoom();
            }
            else
            {

                FightsEventHandler.ChangeZoom();
            }
        }

            if (shooting)
        {
            
            animator.SetLayerWeight(1, Mathf.Lerp(animator.GetLayerWeight(1), 1, 4f * Time.deltaTime));
        }
        else
        {
            
            animator.SetLayerWeight(1, Mathf.Lerp(animator.GetLayerWeight(1), 0, 5f * Time.deltaTime));
        }
        if (inputDir != Vector2.zero) {
            jopa++;
			float targetRotation = Mathf.Atan2 (inputDir.x, inputDir.y) * Mathf.Rad2Deg + cameraT.eulerAngles.y;
			transform.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(transform.eulerAngles.y, targetRotation, ref turnSmoothVelocity, GetModifiedSmoothTime(turnSmoothTime));
            if (running)
            {
                if (y > 0)
                {
                    additiveFloatY = Mathf.Lerp(additiveFloatY, 1, 1.5f * Time.deltaTime);
                }
                else
                {
                    additiveFloatY = Mathf.Lerp(additiveFloatY, -1, 1.5f * Time.deltaTime);
                }

                if (x > 0)
                {
                    additiveFloatX = Mathf.Lerp(additiveFloatX, 1, 1.5f * Time.deltaTime);
                }
                else
                {
                    additiveFloatX = Mathf.Lerp(additiveFloatX, -1, 1.5f * Time.deltaTime);
                }

            }
            else
            {
                additiveFloatY = Mathf.Lerp(additiveFloatY, 0, 1.5f * Time.deltaTime);
                additiveFloatX = Mathf.Lerp(additiveFloatX, 0, 1.5f * Time.deltaTime);
            }
            y += additiveFloatY;
            x += additiveFloatX;
        }

        animator.SetFloat("Horizontal", x);
        animator.SetFloat("Vertical", y);

        
    }

    
    float GetModifiedSmoothTime(float smootTime)
    {
        if (controller.isGrounded)
        {
            return smootTime;
        }

        if (airControlPercent == 0)
        {
            return float.MaxValue;
        }

        return smootTime / airControlPercent;
    }
}
