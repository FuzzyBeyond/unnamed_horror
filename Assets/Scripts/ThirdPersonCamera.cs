﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ThirdPersonCamera : MonoBehaviour {

	public bool lockCursor;
	public float mouseSensitivity = 10;
	public Transform target;
	public float dstFromTarget = 2;
	public Vector2 pitchMinMax = new Vector2 (-40, 85);
    public Vector2 yawMinMax = new Vector2(-50, 50);

    public float rotationSmoothTime = .12f;
	Vector3 rotationSmoothVelocity;
	Vector3 currentRotation;
    Vector3 bodyRotation;

    private bool inZoom = false;
    public float distInZoom;
    public GameObject AimObj;
    public GameObject bulletPlate;
    public GameObject bulletPref;
    public GameObject emptyBulletPref;
    public List<GameObject> bulletsList=new List<GameObject>();
    public int bullIndex;

	float yaw;
    float yaw2;
	float pitch;

	void Start() {
		if (lockCursor) {
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
		}
        
    }

    private void OnEnable()
    {
        FightsEventHandler.onZoomChange += CameraPlaceChange;
        FightsEventHandler.OnChangeWeapon += DrawBullets;
        FightsEventHandler.OnAttack += SmthAboutShoot;
        FightsEventHandler.OnReloadWeapon += ReDrawBullets;
    }

    private void OnDestroy()
    {
        FightsEventHandler.onZoomChange -= CameraPlaceChange;
        FightsEventHandler.OnChangeWeapon -= DrawBullets;
        FightsEventHandler.OnAttack -= SmthAboutShoot;
        FightsEventHandler.OnReloadWeapon -= ReDrawBullets;
    }

    private void SmthAboutShoot()
    {
        bulletsList[bullIndex].SetActive(false);
        bullIndex--;
    }

    void ReDrawBullets(BaseRangeWeapon _weapon)
    {
        if (_weapon != null)
        {
            for (int bullCount = 0; bullCount < _weapon.AmmoCount; bullCount++)
            {
                bulletsList[bullCount].SetActive(true);
                bullIndex++;
            }
        }
    }

    void DrawBullets(BaseRangeWeapon _weapon)
    {
        if (_weapon != null)
        {
            for (int bullCount = 0; bullCount < _weapon.AmmoCount;bullCount++)
            {
                GameObject tmpObj= GameObject.Instantiate(emptyBulletPref, bulletPlate.transform);
                bulletsList.Add(GameObject.Instantiate(bulletPref, tmpObj.transform));
                bullIndex = bulletsList.Count - 1;
                
                
            }
        }
    }

    void CameraPlaceChange(bool _inZoom)
    {
        if (_inZoom != inZoom)
        {
            inZoom = !inZoom;
            if (inZoom)
                target.transform.position = new Vector3(target.transform.position.x + distInZoom, target.transform.position.y, target.transform.position.z);
            else
                target.transform.position = new Vector3(target.transform.position.x - distInZoom, target.transform.position.y, target.transform.position.z);
            AimObj.SetActive(inZoom);
        }
    }

	void LateUpdate () {
		yaw += Input.GetAxis ("Mouse X") * mouseSensitivity;
        pitch -= Input.GetAxis ("Mouse Y") * mouseSensitivity;
		pitch = Mathf.Clamp (pitch, pitchMinMax.x, pitchMinMax.y);

	    currentRotation = Vector3.SmoothDamp (currentRotation, new Vector3 (pitch, yaw), ref rotationSmoothVelocity, rotationSmoothTime);
        transform.eulerAngles = currentRotation;

        
        
        transform.position = new Vector3(target.position.x, target.position.y, target.position.z) - transform.forward * dstFromTarget;

	}

}
