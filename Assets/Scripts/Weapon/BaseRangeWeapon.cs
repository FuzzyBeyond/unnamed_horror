﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseRangeWeapon : MonoBehaviour, IWeapon
{
    public int BaseDamage { get; set; }
    public int AmmoCount { get ; set ; }
    public float ReloadTime { get ; set; }
    public float ShootDelay { get; set; }
    public int RealAmmo { get; set; }

    public float RayDist;

    public Vector3 direction;

    float tmpReloadTime;
    public bool inDelay;

    public bool Ready
    {
        get { return (!reloading && RealAmmo > 0 && !inDelay); }
    }

    public bool reloading;

    private void Awake()
    {
        BaseDamage = 20;
        AmmoCount = 10;
        RealAmmo = AmmoCount;
        ReloadTime = 2;
        ShootDelay = 0.2f;
        reloading = false;
        inDelay = false;
    }

    public void Reload()
    {
        RealAmmo = AmmoCount;
        StartCoroutine("ReloadingTimer");
    }


    public void Fire()
    {
        if (Ready)
        {
            RealAmmo--;
            if (RealAmmo <= 0)
                Reload();
            FightsEventHandler.Attack();
            Ray ray = new Ray(transform.position, transform.TransformDirection(Vector3.back));
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.back) * RayDist, Color.blue, 5.0f);
            //new Ray(transform.position, direction);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                
                //if (hit.collider.tag == "Enemy")
                {
                    Destroy(hit.collider.gameObject); 
                    Debug.Log(hit.collider.gameObject.name);
                }
            }
            StartCoroutine("Delay");
        }
        else
        {
            if (reloading)
            {
                //Debug.Log(tmpReloadTime);
            }
            //Debug.Log("HAha");
        }
    }

    IEnumerator Delay()
    {
        inDelay = true;
        float tmpTime = ShootDelay;
        while (tmpTime > 0)
        {
            tmpTime -= 0.1f;
            yield return new WaitForSecondsRealtime(0.1f);
        }
        inDelay = false;
        yield break;
    }

    IEnumerator ReloadingTimer()
    {
        reloading = true;
        tmpReloadTime = ReloadTime;
        while (tmpReloadTime > 0)
        {
            tmpReloadTime -= 0.1f;
            yield return new WaitForSecondsRealtime(0.1f);
        }
        reloading = false;
        FightsEventHandler.RealoadWepon(this);
        yield break;
    }
}
