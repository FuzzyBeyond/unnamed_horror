﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FightsEventHandler : MonoBehaviour {

    public delegate void FightEventHandler();
    public static event FightEventHandler OnAttack;

    public delegate void WeaponEventHandler(BaseRangeWeapon _weapon);
    public static event WeaponEventHandler OnChangeWeapon;
    public static event WeaponEventHandler OnReloadWeapon;

    public delegate void ZoomEventHandler(bool _inZoom);
    public static event ZoomEventHandler onZoomChange;

    public static void ChangeWeapon(BaseRangeWeapon _weapon)
    {
        OnChangeWeapon(_weapon);
    }

    public static void RealoadWepon(BaseRangeWeapon _weapon)
    {
        OnReloadWeapon(_weapon);
    }

    public static bool inZoom = false;

    public static void Attack()
    {
        OnAttack();
    }

    public static void ChangeZoom()
    {
        inZoom = !inZoom;
        onZoomChange(inZoom);
    }
}
