﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[SerializeField]
public interface IWeapon  {

    int BaseDamage { get; set; }
    float ShootDelay { get; set; }

    void Fire();

}
