﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aiming : MonoBehaviour {

    private ThirdPersonCamera camScript;
    public PlayerController PlayerControllerScript;
    private bool inZoom;

    private void Start()
    {
        camScript = Camera.main.GetComponent<ThirdPersonCamera>();
    }

    void Update () {
        if (FightsEventHandler.inZoom)
        {
            inZoom = true;
            camScript.dstFromTarget = Mathf.Lerp(camScript.dstFromTarget, 0.5f, Time.deltaTime * 3f);
            Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, 90f, Time.deltaTime * 3f);
        }
        else
        {
            inZoom = false;
            camScript.dstFromTarget = Mathf.Lerp(camScript.dstFromTarget, 1.3f, Time.deltaTime * 4f);
            Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, 60f, Time.deltaTime * 4f);
        }
	}
}
