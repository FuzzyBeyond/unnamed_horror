﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class characterCollider : MonoBehaviour {

    public EnemyController EnemyControllerScript;
    public PlayerController PlayerControllerScript;

    void OnTriggerStay(Collider coll)
    {
        if (coll.gameObject.name == "demon@Idle" && PlayerControllerScript.praying)
        {
            EnemyControllerScript.isPraying = true;
        }
        else if (coll.gameObject.name == "demon@Idle" && !PlayerControllerScript.praying)
        {
            EnemyControllerScript.isPraying = false;
        }
    }

    void OnTriggerExit(Collider coll)
    {
        if (coll.gameObject.name == "demon@Idle" && PlayerControllerScript.praying)
        {
            EnemyControllerScript.isPraying = false;
        }
        else if (coll.gameObject.name == "demon@Idle" && !PlayerControllerScript.praying)
        {
            EnemyControllerScript.isPraying = false;
        }
    }
}
