﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class crossScript : MonoBehaviour {

    public GameObject cross;
    public PlayerController PlayerControllerScript;

    public Image crossImage;

    public int prayIntensity;

    public void Start()
    {
        prayIntensity = 99;
        cross.SetActive(false);
        StartCoroutine("prayingCoroutine");
    }

    public void enableCross(bool state)
    {
        cross.SetActive(state);
    }

    IEnumerator prayingCoroutine()
    {
        while (true)
        {
            if (prayIntensity < 100 && prayIntensity >= 0)
            {
                if (PlayerControllerScript.praying)
                {
                    prayIntensity--;
                    prayIntensity = prayIntensity < 0 ? 0 : prayIntensity;
                }
                else
                {
                    prayIntensity += 2;
                    prayIntensity = prayIntensity > 99 ? 99 : prayIntensity;
                }
            }
            crossImage.fillAmount = (float)prayIntensity / 100;
            yield return new WaitForSeconds(0.2f);
        }
    }
    
}
