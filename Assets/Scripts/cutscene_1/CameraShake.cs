using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour
{
	public Transform carTransform;
	
	// How long the object should shake for.
	public float shakeDuration = 0f;
	
	// Amplitude of the shake. A larger value shakes the camera harder.
	public float shakeAmount = 0.7f;
	public float decreaseFactor = 1.0f;
	
	Vector3 originalPos;
	
	void Awake()
	{
		if (carTransform == null)
		{
            carTransform = GetComponent(typeof(Transform)) as Transform;
		}
	}
	
	void OnEnable()
	{
		originalPos = carTransform.localPosition;
	}

	void Update()
	{
		if (shakeDuration > 0)
		{
            carTransform.localPosition = originalPos + Random.insideUnitSphere * shakeAmount;
			
			shakeDuration -= Time.deltaTime * decreaseFactor;
		}
		else
		{
			shakeDuration = 0f;
            carTransform.localPosition = originalPos;
		}
	}
}
