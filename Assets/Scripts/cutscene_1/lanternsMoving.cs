﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lanternsMoving : MonoBehaviour {

    public GameObject lanterns;
    private float startPos = 1.78f;
    private float endPos = -10.2199f;
    private float position;

    void Start()
    {
        position = startPos;
        StartCoroutine("moveLanterns");
    }

    IEnumerator moveLanterns()
    {
        while (true)
        {
            position -= 0.06f;
            lanterns.transform.position = new Vector3(lanterns.transform.position.x, lanterns.transform.position.y, position);
            if (position <= endPos)
            {
                position = startPos;
            }
            yield return new WaitForSeconds(0.01f);
        }
    }
}
