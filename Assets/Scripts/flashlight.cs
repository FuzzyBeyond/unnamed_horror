﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flashlight : MonoBehaviour {

    bool enabled;
    public Light flashlightLight;
    public GameObject flashlightObj;
    private GameObject flashlight_child;

    private void Start()
    {
        flashlight_child = flashlightObj.transform.Find("Matrix_1").gameObject;
    }
    
    void Update () {
        if (Input.GetKeyDown(KeyCode.G))
        {
            enabled = !enabled;
        }

        if (enabled)
        {
            flashlight_child.GetComponent<Renderer>().material.SetColor("_EmissionColor", new Color(2.199F, 2.199F, 2.199F, 2.199F));
            flashlightLight.intensity = 1.26f;
        }
        else
        {
            flashlight_child.GetComponent<Renderer>().material.SetColor("_EmissionColor", new Color(0F, 0F, 0F, 0F));
            flashlightLight.intensity = 0;
        }
    }
}
