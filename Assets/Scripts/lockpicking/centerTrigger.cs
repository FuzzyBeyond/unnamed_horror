﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class centerTrigger : MonoBehaviour {

    public bool isEntered;

    private void OnTriggerEnter(Collider obj)
    {
        isEntered = true;
    }

    private void OnTriggerExit(Collider other)
    {
        isEntered = false;
    }
}
