﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pinMovement : MonoBehaviour {
    
    public GameObject lowerPin_1;
    private Animator anim;

    public int pinNum;

    private void Start()
    {
        anim = this.GetComponent<Animator>();
    }

    public void setPin(bool reversed)
    {
        if (reversed)
        {
            if (pinNum >= 1)
            {
                pinNum--;
                switch (pinNum)
                {
                    case 0:
                        anim.Play("openpin_1", 0, 0.25f);
                        break;
                    case 1:
                        anim.Play("openpin_2", 0, 0.25f);
                        break;
                    case 2:
                        anim.Play("openpin_3", 0, 0.25f);
                        break;
                    case 3:
                        anim.Play("openpin_4", 0, 0.25f);
                        break;
                    case 4:
                        anim.Play("openpin_5", 0, 0.25f);
                        anim.SetFloat("speed", 0);
                        break;
                }
                pinNum = 0;
            }
        }
        else
        {
            pinNum++;
            switch (pinNum)
            {
                case 1:
                    anim.Play("closepin_1", 0, 0.25f);
                    break;
                case 2:
                    anim.Play("closepin_2", 0, 0.25f);
                    break;
                case 3:
                    anim.Play("closepin_3", 0, 0.25f);
                    break;
                case 4:
                    anim.Play("closepin_4", 0, 0.25f);
                    break;
                case 5:
                    anim.Play("closepin_5", 0, 0.25f);
                    StartCoroutine("rotateReel");
                    break;
            }
        }
    }

    IEnumerator rotateReel()
    {
        anim.SetFloat("speed", 0);
        yield return new WaitForSeconds(2f);
        anim.Play("rotateReel", 2, 0.25f);
        StopCoroutine("rotateReel");
    }
}
