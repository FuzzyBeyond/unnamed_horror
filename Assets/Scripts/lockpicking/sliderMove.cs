﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sliderMove : MonoBehaviour {
    
    private Animator anim;
    public GameObject center;

    private void Start()
    {
        anim = this.GetComponent<Animator>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (this.GetComponent<pinMovement>().pinNum < 5)
            {
                if (center.GetComponent<centerTrigger>().isEntered)
                {
                    center.GetComponent<centerTrigger>().isEntered = false;
                    StartCoroutine("lockDelay");
                    this.GetComponent<pinMovement>().setPin(false);
                }
                else
                {
                    StartCoroutine("lockDelay");
                    this.GetComponent<pinMovement>().setPin(true);
                }
            }
        }
    }

    IEnumerator lockDelay()
    {
        anim.SetFloat("speed", 0);
        yield return new WaitForSeconds(2f);
        if (this.GetComponent<pinMovement>().pinNum < 5)
        {
            anim.SetFloat("speed", 1);
        }
        StopCoroutine("lockDelay");
    }
}
