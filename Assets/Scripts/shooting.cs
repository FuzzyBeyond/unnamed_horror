﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shooting : MonoBehaviour {

    public GameObject shootigVFXobj;
    private Animator anim;
    private Animator animemer;
    private PlayerController PlayerControllerScript;

    private void Start()
    {
        anim = shootigVFXobj.GetComponent<Animator>();
        animemer = this.GetComponent<Animator>();
        PlayerControllerScript = this.GetComponent<PlayerController>();
        
    }

    private void OnEnable()
    {
        FightsEventHandler.OnAttack += Shoot;
    }

    private void OnDestroy()
    {
        FightsEventHandler.OnAttack -= Shoot;
    }

    void Shoot()
    {
        anim.Play("shoot", -1, 0f);
        animemer.Play("Shooting", -1, 0f);
    }
}
