﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class subtitlesManager : MonoBehaviour {

    [System.Serializable]
    public struct phraseLine
    {
        [TextArea] public string phraseText;
        public int interval;
    }

    [SerializeField] public List<phraseLine> Praying = new List<phraseLine>(); // Молитва
    [SerializeField] public List<phraseLine> Fear = new List<phraseLine>(); // Страх

    void Start () {
    }
}
