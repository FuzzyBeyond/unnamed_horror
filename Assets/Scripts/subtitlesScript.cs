﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class subtitlesScript : MonoBehaviour {

    public Text textObject;

    public void showLine(string lineText)
    {
        textObject.text = lineText;
    }

    IEnumerator subtitlesCoroutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.1f);
        }
    }
}
